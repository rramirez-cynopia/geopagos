<?php

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', realpath(dirname(__FILE__)) . DS);
define('SYSTEM_PATH', ROOT . 'system' . DS);
define('APP_PATH', ROOT . 'app' . DS);
define('STORAGE_PATH', ROOT . 'storage' . DS);
define('PUBLIC_PATH', ROOT . 'public' . DS);
define('BOOTSTRAP_PATH', PUBLIC_PATH . 'bootstrap' . DS);
define('UTILITY_PATH', APP_PATH . 'utility' . DS);
define('CONTROLLER_PATH', APP_PATH . 'controller' . DS);
define('MODEL_PATH', APP_PATH . 'model' . DS);
define('VIEWS_PATH', APP_PATH . 'view' . DS);

// REQUERIR AL ARCHIVO DE CONFIGURACION DEL SISTEMA
require_once 'system/config.php';