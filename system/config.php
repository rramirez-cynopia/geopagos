<?php

function __agregarArchivos($direccion, $ruta, $rutas)
{
	$_rutas = (empty($rutas)) ? [] : $rutas;

	if($ruta!= null && !empty($ruta)) {
		foreach ($ruta as $_llave => $_archivo) {
			if($_archivo!="" && $_archivo!="." && $_archivo!="..") {
				$_rutas[] = $direccion . '' . $_archivo;
			}
		}
	}

	return $_rutas;
}

function __autoCarga() 
{
	$_load_system = [];
	$_system_archivos = scandir(SYSTEM_PATH);
	$_utility_archivos = scandir(UTILITY_PATH);
	$_controller_archivos = scandir(CONTROLLER_PATH);
    $_model_archivos = scandir(MODEL_PATH);

    // COLOCAR DE PRIMERO LA CLASE CONTROLADOR
    array_unshift($_controller_archivos, "Controlador.php");
    $_controller_archivos = array_unique($_controller_archivos);

    // COLOCAR DE PRIMERO LA CLASE MODELO
    array_unshift($_model_archivos, "Modelo.php");
    $_model_archivos = array_unique($_model_archivos);

	$_load_system = __agregarArchivos(SYSTEM_PATH, $_system_archivos, $_load_system);
	$_load_system = __agregarArchivos(UTILITY_PATH, $_utility_archivos, $_load_system);
    $_load_system = __agregarArchivos(MODEL_PATH, $_model_archivos, $_load_system);
    $_load_system = __agregarArchivos(CONTROLLER_PATH, $_controller_archivos, $_load_system);

    foreach ($_load_system as $_llave => $_archivo) {
        $_archivo_format = str_replace(ROOT, "", $_archivo);
        
        require_once $_archivo_format;
    }
}

function __run() {
    $controlador_nombre = "Usuario";
    $accion = "index";

    if(isset($_GET['url'])) {
        $url = filter_input(INPUT_GET, "url", FILTER_SANITIZE_URL);
        $url = explode('/', $url);

        $controlador_nombre = ucfirst(strtolower(array_shift($url)));
        $accion = strtolower(array_shift($url));
        $argumentos = $url;

        $controlador = $controlador_nombre . 'Controlador';
        $controlador_ruta = CONTROLLER_PATH . $controlador . '.php';

        if(is_readable($controlador_ruta)) {
            $controlador_refleccion = new ReflectionClass("\\controller\\$controlador");
            $controlador_instancia = $controlador_refleccion->newInstance();
            $accion = "accion" . ucfirst($accion);

            if(isset($argumentos)) {
                call_user_func_array([$controlador_instancia, $accion], $argumentos);
            } else {
                call_user_func([$controlador_instancia, $accion]);
            }
        }
    } else {
        $_controlador = new controller\Controlador();
        $_controlador->redireccionar("usuario/index");
    }
}



// INICIAR SISTEMA
__autoCarga();
__run();