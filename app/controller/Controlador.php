<?php

namespace controller;

use utility\Utilitario;
use Exception;

/**
* CLASE Controlador
* CLASE DE CONTROALDORES
*/
class Controlador 
{
    private $_utilitario;
    private $_marco = 'principal';
    private $_contenido;
    
    function __construct()
    {
        $this->_utilitario = new Utilitario();
    }
    
    public function renderizar($vista, $parametros = []) 
    {
        $_clase = $this;
        $_controlador_nombre = $this->_utilitario->getNombreControlador($_clase);
        $_controlador_nombre_simple = str_replace("controlador", "", $_controlador_nombre);
        $_vista_ruta = VIEWS_PATH . strtolower($_controlador_nombre_simple) . DS . $vista . '.php';
        
        if(is_readable($_vista_ruta)) {
            $this->setContenidoMarco($_vista_ruta, $parametros);
        } else {
            throw new \Exception('ERROR VISTA');
        }
    }
    
    public function redireccionar($ruta = false) 
    {
        $_ruta_completa = $this->getRutaNavegador($ruta);

        if($_ruta_completa) {
            header('Location: ' . $_ruta_completa);
            exit;
        }
    }
    
    public function setContenidoMarco($vista_ruta, $parametros = []) {
        $marcho = $this->_marco;
        $marcho_ruta = VIEWS_PATH . 'layouts' . DS . $marcho . ".php";
        $_contenido = $vista_ruta;
        
        // EXTRACCION DE PARAMETROS COMO VARIABLES
        extract($parametros);
        // INCLUSION DE LA VISTA
        include($marcho_ruta);
    }

    public function getRutaNavegador($ruta)
    {
        $_uri_arreglo = explode("/", $_SERVER['REQUEST_URI']);
        $_uri_arreglo_primero = $_uri_arreglo[1];
        $_ruta_completa = "/" . $ruta;
        $_ruta_proyecto = str_replace("\\" , "/", str_replace("\app", "", dirname(dirname(__FILE__))));
        $_ruta_proyecto_final = split("/", $_ruta_proyecto)[3];

        if($_uri_arreglo_primero!=null && !empty($_uri_arreglo_primero)) {
            if(strpos($ruta, $_uri_arreglo_primero) !== false) {
                $_ruta_completa = "/" . $ruta;
            } else {
                $_ruta_completa = "/" . $_uri_arreglo_primero . "/" . $ruta;
            }

            $_ruta_completa_primero = split("/", $_ruta_completa)[1];

            if($_ruta_completa_primero !== $_ruta_proyecto_final) {
                $_ruta_completa = "/" . $ruta;
            }
        }
        
        return $_ruta_completa;
    }

}
