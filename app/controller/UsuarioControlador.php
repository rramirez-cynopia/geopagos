<?php

namespace controller;

use model\Usuario;
use utility\Utilitario;

/**
* CLASE Utilitario
* CLASE DE ELEMENTOS UTILITARIOS
*/
class UsuarioControlador extends Controlador
{
    private $_utilitario;
    private $_ruta;
    
    function __construct()
    {
        parent::__construct();
        $this->_utilitario = new Utilitario();
    }
    
    public function accionIndex() 
    {
        $modelo = new Usuario();
        $datos = $modelo->consultarTodos();
        
        $this->renderizar("index", [
            "modelo" => $datos
        ]);
    }
    
    public function accionCrear() 
    {
        $modelo = new Usuario();
        $_resultado = false;
        $_pagina_index = $this->getRutaNavegador("usuario/index");

        if(isset($_POST)) {
            $_datos_post = $_POST;
            
            if($_datos_post!=null && !empty($_datos_post)) {
                $_codigo_usuario = $this->_utilitario->genCodigoAleatorio();
                $modelo->setCodigoUsuario($_codigo_usuario);

                foreach ($_datos_post as $_propiedad => $_valor) {
                    // FORMATO DE LA PROPIEDAD EN LA CLASE
                    $_propiedad_format_setter = "set" . ucfirst($_propiedad);
                    
                    $modelo->$_propiedad_format_setter($_valor);
                }

                $_resultado = $modelo->insertar();

                $this->redireccionar("usuario/index");
            }
        }
        
        $this->renderizar("crear", [
            "modelo" => $modelo,
            "pagina_index" => $_pagina_index
        ]);
    }
    
    public function accionEditar($identificador) 
    {
        $modelo = new Usuario();
        $usuario = $modelo->consultarUno(["codigousuario" => $identificador]);
        $_pagina_index = $this->getRutaNavegador("usuario/index");
        $_resultado = false;
        
        if(isset($_POST)) {
            $_datos_post = $_POST;
            
            if($_datos_post!=null && !empty($_datos_post)) {
                foreach ($_datos_post as $_propiedad => $_valor) {
                    // FORMATO DE LA PROPIEDAD EN LA CLASE
                    $_propiedad_format_setter = "set" . ucfirst($_propiedad);
                    
                    $usuario->$_propiedad_format_setter($_valor);
                }
                
                $_resultado = $usuario->modificar();

                $this->redireccionar("usuario/index");
            }
        }
        
        $this->renderizar("editar", [
            "modelo" => $usuario,
            "pagina_index" => $_pagina_index
        ]);
    }
    
    public function accionEliminar($identificador) 
    {
        $modelo = new Usuario();
        $usuario = $modelo->consultarUno(["codigousuario" => $identificador]);
        $_pagina_index = $this->getRutaNavegador("usuario/index");
        $_resultado = false;

        if(isset($_POST)) {
            $_datos_post = $_POST;
            
            if($_datos_post!=null && !empty($_datos_post)) {
                $_resultado = $usuario->eliminar();

                $this->redireccionar("usuario/index");
            }
        }

        $this->renderizar("eliminar", [
            "modelo" => $usuario,
            "pagina_index" => $_pagina_index
        ]);
    }

    public function accionFavoritos($identificador) 
    {
        $this->redireccionar("favorito/index/" . $identificador);
    }
}
