<?php

namespace controller;

use model\Usuario;
use model\Favorito;
use utility\Utilitario;

/**
* CLASE Utilitario
* CLASE DE ELEMENTOS UTILITARIOS
*/
class FavoritoControlador extends Controlador
{
    private $_utilitario;
    private $_ruta;
    
    function __construct()
    {
        parent::__construct();
        $this->_utilitario = new Utilitario();
    }
    
    public function accionIndex($identificador) 
    {
        $modelo = new Favorito();
        $usuario = new Usuario();
        $datos = $modelo->consultarTodos(["codigousuario" => $identificador]);
        $modelo_usuario = $usuario->consultarUno(["codigousuario" => $identificador]);

        if(count($datos) > 0) {
            $_datos = [];

            foreach ($datos as $_llave => $_usuario) {
                $_usuario_instancia = new Usuario();
                $_usuario_datos = $_usuario_instancia->consultarUno(["codigousuario" => $_usuario->getCodigoUsuarioFavorito()]);

                $_datos[] = $_usuario_datos;
            }

            $datos = $_datos;
        }
        
        $this->renderizar("index", [
            "modelo" => $datos,
            "usuario" => $modelo_usuario
        ]);
    }

    public function accionCrear($identificador) 
    {
        $modelo = new Favorito();
        $usuario = new Usuario();
        $modelo_usuario = $usuario->consultarUno(["codigousuario" => $identificador]);
        $_resultado = false;
        $_pagina_index = $this->getRutaNavegador("favorito/index/" . $identificador);

        if(isset($_POST)) {
            $_datos_post = $_POST;
            
            if($_datos_post!=null && !empty($_datos_post)) {
                $modelo->setCodigoUsuario($identificador);

                foreach ($_datos_post as $_propiedad => $_valor) {
                    // FORMATO DE LA PROPIEDAD EN LA CLASE
                    $_propiedad_format_setter = "set" . ucfirst($_propiedad);
                    
                    $modelo->$_propiedad_format_setter($_valor);
                }

                $_resultado = $modelo->agregar();

                $this->redireccionar("favorito/index/" . $identificador);
            }
        }

        $datos = $usuario->consultarTodos();
        $datos_favoritos = $modelo->consultarTodos();
        $_usuarios_a_favoritos = [];

        foreach ($datos as $_llave => $_usuario) {
            $_usuario_codigo = $_usuario->getCodigoUsuario();

            if($identificador != $_usuario_codigo) {
                if(count($datos_favoritos) > 0) {
                    $_verificar_existencia = true;

                    foreach ($datos_favoritos as $_llave_favorito => $_favorito) {
                        $_usuario_codigo_favorito = $_favorito->getCodigoUsuarioFavorito();

                        if($_usuario_codigo == $_usuario_codigo_favorito) {
                            $_verificar_existencia = false;
                        }
                    }

                    if($_verificar_existencia) {
                        $_usuarios_a_favoritos[] = $_usuario;
                    }
                } else {
                    $_usuarios_a_favoritos[] = $_usuario;
                }
            }
        }
        
        $this->renderizar("crear", [
            "modelo" => $modelo,
            "usuario" => $modelo_usuario,
            "usuarios" => $_usuarios_a_favoritos,
            "pagina_index" => $_pagina_index
        ]);
    }

    public function accionEliminar($identificador) 
    {
        $modelo = new Favorito();
        
        echo "Eliminar";
    }
}