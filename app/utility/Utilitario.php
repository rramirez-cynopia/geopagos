<?php

namespace utility;

/**
* CLASE Utilitario
* CLASE DE ELEMENTOS UTILITARIOS
*/
class Utilitario
{
	function __construct()
	{
		
	}

	public function verificarArchivo($archivo)
	{
		// RUTA RELATIVA DEL ARCHIVO
		$_archivo = STORAGE_PATH . $archivo . ".json";
		$_archivo_existe = file_exists($_archivo);

		if($_archivo_existe) {
			//echo "Archivo " . $archivo . " existe.";
		} else {
			// CREACION DEL ARCHIVO SI NO EXISTE
			$this->crearArchivo($archivo);
		}
	}

	public function crearArchivo($archivo)
	{
		// RUTA RELATIVA DEL ARCHIVO | APERTURA DEL ARCHIVO
                // RUTA RELATIVA DEL ARCHIVO
		$_archivo = STORAGE_PATH . $archivo . ".json";
                
		$_archivo_abrierto = fopen($_archivo, 'w');
		fwrite($_archivo_abrierto, "[]");
		fclose($_archivo_abrierto);
	}

	public function agregarArchivo($archivo, $contenido)
	{
		$this->verificarArchivo($archivo);

		$_json = "[]";
		// RUTA RELATIVA DEL ARCHIVO
		$_archivo = STORAGE_PATH . $archivo . ".json";

		if($contenido!=null && !empty($contenido)) {
			$_json = json_encode($contenido);
		}

		// ESCRITURA (REEMPLAZO) DEL CONTENIDO AL ARCHIVO
		file_put_contents($_archivo, $_json);
	}

	public function obtenerArchivo($archivo)
	{
		$this->verificarArchivo($archivo);

		$_json = [];
		// RUTA RELATIVA DEL ARCHIVO
		$_archivo = STORAGE_PATH . $archivo . ".json";

		// OBTENER CONTENIDO DEL ARCHIVO
		$_archivo_contenido = file_get_contents($_archivo);

		// VERIFICAR SI EL CONTENIDO ESTA VACIO
		if($_archivo_contenido!=null && !empty($_archivo_contenido)) {
			// ASIGNAR CONTENIDO A FORMATO JSON
			$_json = json_decode($_archivo_contenido, true);
		}

		return $_json;
	}

	public function getClaseComoArreglo($clase)
	{
		$_objeto = [];
		$refleccion = new \ReflectionClass($clase);
		// OBTENER PROPIEDADES PROTEGIDAS EN LA INSTANCIA DE LA CLASE
		$propiedades = $refleccion->getProperties(\ReflectionProperty::IS_PROTECTED);

		// RECORRIDO DE LAS PROPIEDADES DE LA INSTANCIA
		foreach($propiedades as $propiedad) {
			$_propiedad_nombre = $propiedad->getName();
			$_propiedad_objeto = $refleccion->getProperty($_propiedad_nombre);
			$_propiedad_objeto->setAccessible(true);
			$_propiedad_valor = $_propiedad_objeto->getValue($clase);

			// FORMATO DE LA PROPIEDAD EN LA CLASE (SOLO MINUSCULAS | SOLO LETRAS)
			$_propiedad_nombre_formato = $this->getPropiedadFormat($_propiedad_nombre);

			// ASIGNACION DE LAS PROPIEDADES FORMATEADAS Y LOS VALORES DE LA INSTANCIA AL OBJETO FINAL
		    $_objeto[$_propiedad_nombre_formato] = $_propiedad_valor;
		}

		return $_objeto;
	}

	public function getPropiedadFormat($propiedad) 
	{
		$_propiedad_nombre_minusculas = strtolower($propiedad);
		$_propiedad_nombre_solo_letras = preg_replace('/[^A-Za-z]/', '', $_propiedad_nombre_minusculas);

		// OBTENER EL VALOR FORMATEADO
		return $_propiedad_nombre_solo_letras;
	}

	public function getNombreAlmacenTemporal($clase) 
	{
		$_clase_nombre = get_class($clase);
        $_clase = new \ReflectionClass($_clase_nombre);
        $_clase_nombre_clase = $_clase->getName();
        $_clase_nombre_espacio = $_clase->getNamespaceName();
        
        if(strpos($_clase_nombre_clase, $_clase_nombre_espacio . "\\") !== false) {
            $_clase_nombre_clase = str_replace($_clase_nombre_espacio . "\\", "", $_clase_nombre_clase);
        }
                
		$_clase_nombre_min = strtolower($_clase_nombre_clase);
		$_clase_almacen_temporal = $_clase_nombre_min . 's';

		return $_clase_almacen_temporal;
	}
        
    public function getNombreControlador($clase) 
	{
		$_clase_nombre = get_class($clase);
        $_clase = new \ReflectionClass($_clase_nombre);
        $_clase_nombre_clase = $_clase->getName();
        $_clase_nombre_espacio = $_clase->getNamespaceName();
        
        if(strpos($_clase_nombre_clase, $_clase_nombre_espacio . "\\") !== false) {
            $_clase_nombre_clase = str_replace($_clase_nombre_espacio . "\\", "", $_clase_nombre_clase);
        }
                
		$_clase_nombre_min = strtolower($_clase_nombre_clase);
		$_clase_controlador = $_clase_nombre_min;

		return $_clase_controlador;
	}

	public function getClaseAnotacion($clase)
	{       
	    $refleccion = new \ReflectionClass($clase);
	    $documentacion = $refleccion->getDocComment();

	    // OBTENER SOLO LOS VALORES INCLUIDOS EN LA EXPRESION REGULAR
	    preg_match_all('#@(.*?)\n#s', $documentacion, $anotaciones);

	    return $anotaciones[1];
	}

	public function getClaseIdParametro($clase)
	{
		$_anotaciones = $this->getClaseAnotacion($clase);
		$_campo_id = "";

		// RECORRIDO DE LAS ANOTACIONES DE LA CLASE
		foreach($_anotaciones as $_anotacion) {
			if($_anotacion!=null && !empty($_anotacion)) {

				// VERIFICACION DE LA ANOTACION (ID)
				if(strpos($_anotacion, "param id:") !== false) {
					// ASIGNAR CAMPO DE LA CLASE CON LA ANOTACION (ID)
					$_campo_id = str_replace("param id:", "", $_anotacion);
					break;
				}
			}
		}

		return $_campo_id;
	}

	public function genCodigoAleatorio($longitud = null)
	{
		if(!isset($longitud) || empty($longitud)) {
			$longitud = 32;
		}

		$codigo = "";
	    $codigoAlfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    $codigoAlfabeto.= "abcdefghijklmnopqrstuvwxyz";
	    $codigoAlfabeto.= "0123456789";
	    $maximo = strlen($codigoAlfabeto); // edited

	    for ($i=0; $i < $longitud; $i++) {
	        $codigo .= $codigoAlfabeto[rand(0, $maximo-1)];
	    }

	    return $codigo;
	}

	public function fechaValidaCadenaCaracteres($fecha, $formato = 'Y-m-d')
	{
	    $d = DateTime::createFromFormat($formato, $fecha);

	    return $d && $d->format($formato) == $fecha;
	}

	public function verifiarSiEsFecha($fecha) 
	{
    	return (bool)strpbrk($fecha, 1234567890) && strtotime($fecha);
	}

	public function setFechaCorrecta($fecha)
	{
		$_fecha_hoy = date('Y-m-d');

        if($fecha != null && !empty($fecha)) {
            if(is_string($fecha)) {
                if($this->utilitario->fechaValidaCadenaCaracteres($fecha)) {
                    $_fecha = strtotime($fecha);
                    $_fecha_hoy = date('Y-m-d', $_fecha);
                }
            } else if($this->utilitario->verifiarSiEsFecha($fecha)) {
                $_fecha_hoy = $fecha;
            }
        }

        return $_fecha_hoy;
	}

	public function setImporteCorrecto($importe)
	{
		$_importe = 1;

        if($importe != null && !empty($importe)) {
            if((int)$importe > 0) {
                $_importe = (int)$importe;
            }
        }

        return $_importe;
	}

	public function validarFavoritos($favorito)
	{
		if($favorito != null && !empty($favorito)) {
			$_codigousuario = $favorito->getCodigoUsuario();
			$_codigousuariofavorito = $favorito->getCodigoUsuarioFavorito();

			if($_codigousuario != null && !empty($_codigousuario)) {
				if($_codigousuariofavorito != null && !empty($_codigousuariofavorito)) {
					if($_codigousuario == $_codigousuariofavorito) {
						throw new Exception("Los Códigos son Iguales.");
					}
				}
			}
		}
	}
}