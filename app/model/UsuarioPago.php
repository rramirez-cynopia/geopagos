<?php

namespace model;

/**
* CLASE UsuarioPago
*
*/
class UsuarioPago extends Modelo
{
	protected $_codigopago;
    protected $_codigousuario;
    
    public function __construct() 
    {
       parent::__construct();
    }

    /*
    *	GET | SET codigopago
    */
    public function getCodigoPago()
    {
        return $this->_codigopago;
    }

    public function setCodigoPago($codigopago)
    {
        if($codigopago == null || empty($codigopago)) {
            throw new Exception("El Código de Pago es Invalido.");
        }

        $this->_codigopago = $codigopago;
    }

    /*
    *   GET | SET codigousuario
    */
    public function getCodigoUsuario()
    {
        return $this->_codigousuario;
    }

    public function setCodigoUsuario($codigousuario)
    {
        if($codigousuario == null || empty($codigousuario)) {
            throw new Exception("El Código de Usuario es Invalido.");
        }

        $this->_codigousuario = $codigousuario;
    }
}