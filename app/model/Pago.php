<?php

namespace model;

/**
* CLASE Pago
*
*/
class Pago extends Modelo
{
    
	protected $_codigopago;
    protected $_importe;
    protected $_fecha;
    
    public function __construct() 
    {
       parent::__construct();
    }

    public function Favorito($importe, $fecha) 
    {
       parent::__construct();

       $this->_codigopago = $this->_utilitario->genCodigoAleatorio();
       $this->_importe = $this->utilitario->setImporteCorrecto($importe);
       $this->_fecha = $this->utilitario->setFechaCorrecta($fecha);
    }

    /*
    *	GET | SET codigopago
    */
    public function getCodigoPago()
    {
        return $this->_codigopago;
    }

    public function setCodigoPago($codigopago)
    {
        if($codigopago == null || empty($codigopago)) {
            $codigopago = $this->_utilitario->genCodigoAleatorio();
        }

        $this->_codigopago = $codigopago;
    }

    /*
    *	GET | SET importe
    */
    public function getImporte()
    {
        return $this->_importe;
    }

    public function setImporte($importe)
    {
        $_importe = $this->utilitario->setImporteCorrecto($importe);

        $this->_importe = $_importe;
    }

    /*
    *	GET | SET fecha
    */
    public function getFecha()
    {
        return $this->_fecha;
    }

    public function setFecha($fecha)
    {
        $_fecha_hoy = $this->utilitario->setFechaCorrecta($fecha);

        $this->_fecha = $_fecha_hoy;
    }
}