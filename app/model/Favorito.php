<?php

namespace model;

use utility\Utilitario;

/**
* CLASE Favorito
*
*/
class Favorito extends Modelo
{
    
    protected $_codigousuario;
    protected $_codigousuariofavorito;
    
    public function __construct() 
    {
       parent::__construct();
    }

    public function Favorito($codigousuario, $codigousuariofavorito) 
    {
       parent::__construct();

       $_favorito = $this;
       $utilitario = new Utilitario();
       $utilitario->validarFavoritos($_favorito);

       $this->_codigousuario = $codigousuario;
       $this->_codigousuariofavorito = $codigousuariofavorito;
    }

    /*
    *	GET | SET codigousuario
    */
    public function getCodigoUsuario()
    {
        return $this->_codigousuario;
    }

    public function setCodigoUsuario($codigousuario)
    {
        $this->_codigousuario = $codigousuario;

        $_favorito = $this;
        $utilitario = new Utilitario();
        $utilitario->validarFavoritos($_favorito);
    }
    
    /*
    *	GET | SET codigousuario
    */
    public function getCodigoUsuarioFavorito()
    {
        return $this->_codigousuariofavorito;
    }

    public function setCodigoUsuarioFavorito($codigousuariofavorito)
    {
        $this->_codigousuariofavorito = $codigousuariofavorito;

        $_favorito = $this;
        $utilitario = new Utilitario();
        $utilitario->validarFavoritos($_favorito);
    }
}
