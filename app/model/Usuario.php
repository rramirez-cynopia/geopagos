<?php

namespace model;

use utility\Utilitario;

/**
* CLASE Usuario
*
* @param id:$_codigousuario
* @param int:[>=(18)]:$_edad
*/
class Usuario extends Modelo
{

    protected $_codigousuario;
    protected $_usuario;
    protected $_clave;
    protected $_edad;

    public function __construct() 
    {
       parent::__construct();
    }

    public function Usuario($codigousuario = null, $usuario, $clave, $edad) 
    {
       parent::__construct();
       $utilitario = new Utilitario();
       $this->_codigousuario = (empty($codigousuario)) ? $utilitario->genCodigoAleatorio() : $codigousuario;
       $this->_usuario = $usuario;
       $this->_clave = $clave;
       $this->_edad = ((int)$edad < 18) ? 18 : (int)$edad;
    }

    /*
    *	GET | SET codigousuario
    */
    public function getCodigoUsuario()
    {
        return $this->_codigousuario;
    }

    public function setCodigoUsuario($codigousuario)
    {
        $utilitario = new Utilitario();
        $this->_codigousuario = (empty($codigousuario)) ? $utilitario->genCodigoAleatorio() : $codigousuario;
    }

    /*
    *	GET | SET usuario
    */
    public function getUsuario()
    {
        return $this->_usuario;
    }

    public function setUsuario($usuario)
    {
        $utilitario = new Utilitario();
        if($usuario == null || empty($usuario)) {
            $usuario = $utilitario->genCodigoAleatorio(8);
        }

        $this->_usuario = $usuario;
    }

    /*
    *	GET | SET clave
    */
    public function getClave()
    {
        return $this->_clave;
    }

    public function setClave($clave)
    {
        $utilitario = new Utilitario();
        if($clave == null || empty($clave)) {
            $clave = $utilitario->genCodigoAleatorio(8);
        }

        $this->_clave = $clave;
    }

    /*
    *	GET | SET edad
    */
    public function getEdad()
    {
        return $this->_edad;
    }

    public function setEdad($edad)
    {
        $_edad = 18;

        if($edad != null && !empty($edad)) {
            if((int)$edad > 18) {
                $_edad = (int)$edad;
            }
        }

        $this->_edad = $_edad;
    }
}