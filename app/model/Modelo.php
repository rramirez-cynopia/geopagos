<?php

namespace model;

use utility\Utilitario;

/**
* CLASE Modelo
* CLASE DE ELEMENTOS RELACIONADOS CON LA PERSISTENCIA DE DATOS
*/
class Modelo
{
	private $_utilitario;

	function __construct()
	{
		$this->_utilitario = new Utilitario();
	}

	public function consultarUno($filtro = null) 
	{
		$_resultado = $this->_consultar("uno", $filtro);

		return $_resultado;
	}

	public function consultarTodos($filtro = null) 
	{
		$_resultado = $this->_consultar("todos", $filtro);

		return $_resultado;
	}

	public function insertar() 
	{
		$_clase = $this;
		// OBTENER INFORMACION DEL REGISTRO INSTANCIADO
		$_resultado = $this->_buscarCoincidenciaId($_clase);
		// CLASE INSTANCIADA COMO ARREGLO
		$_clase_objeto = $this->_utilitario->getClaseComoArreglo($_clase);
		// RETORNO DE LA FUNCION
		$_retorno = false;

		if($_resultado!=null && !empty($_resultado)) {
			if(array_key_exists("almacen", $_resultado)) {
				var_dump($_resultado);
				$_almacen = $_resultado["almacen"];
				$_registro_llave = $_resultado["posicion_registro"];
				$_clase_almacen_temporal = $_resultado["nombre_almacen"];
				$_encontrado = $_resultado["encontrado"];

				if(!$_encontrado) {
					// AGREGAR NUEVO ELEMENTO EN EL ALMACENAMIENTO
					array_push($_almacen, $_clase_objeto);

					// GUARDAR EN ARCHIVO
					$this->_utilitario->agregarArchivo($_clase_almacen_temporal, $_almacen);

					$_retorno = true;
				}
			}
		}
	} 

	public function agregar() 
	{
		$_clase = $this;
		// OBTENER INFORMACION DEL REGISTRO INSTANCIADO
		$_resultado = $this->_buscarCoincidenciaId($_clase);


		// OBTENER NOMBRE DEL ALMACEN DE DATOS
		$_clase_almacen_temporal = $this->_utilitario->getNombreAlmacenTemporal($_clase);
		// CLASE INSTANCIADA COMO ARREGLO
		$_clase_objeto = $this->_utilitario->getClaseComoArreglo($_clase);
		// OBETNER CONTENIDO DEL ARCHIVO
		$_almacen = $this->_utilitario->obtenerArchivo($_clase_almacen_temporal);
		// RESULTADO
		$_resultado = [
			"nombre_almacen" => $_clase_almacen_temporal,
			"almacen" => $_almacen
		];
		// RETORNO DE LA FUNCION
		$_retorno = false;

		if($_resultado!=null && !empty($_resultado)) {
			if(array_key_exists("almacen", $_resultado)) {
				var_dump($_resultado);
				$_almacen = $_resultado["almacen"];
				$_clase_almacen_temporal = $_resultado["nombre_almacen"];

				// AGREGAR NUEVO ELEMENTO EN EL ALMACENAMIENTO
				array_push($_almacen, $_clase_objeto);

				// GUARDAR EN ARCHIVO
				$this->_utilitario->agregarArchivo($_clase_almacen_temporal, $_almacen);

				$_retorno = true;
			}
		}
	}

	public function modificar() 
	{
		$_clase = $this;
		// OBTENER INFORMACION DEL REGISTRO INSTANCIADO
		$_resultado = $this->_buscarCoincidenciaId($_clase);
		// CLASE INSTANCIADA COMO ARREGLO
		$_clase_objeto = $this->_utilitario->getClaseComoArreglo($_clase);
		// RETORNO DE LA FUNCION
		$_retorno = false;

		if($_resultado!=null && !empty($_resultado)) {
			if(array_key_exists("almacen", $_resultado)) {
				$_almacen = $_resultado["almacen"];
				$_registro_llave = $_resultado["posicion_registro"];
				$_clase_almacen_temporal = $_resultado["nombre_almacen"];

				if(!is_null($_registro_llave) && is_int($_registro_llave)) {
					// EDICION DEL REGISTRO EN EL ALMACEN
					$_almacen[$_registro_llave] = $_clase_objeto;

					// GUARDAR EN ARCHIVO
					$this->_utilitario->agregarArchivo($_clase_almacen_temporal, $_almacen);
					
					$_retorno = true;
				}
			}
		}

		return $_retorno;
	}

	public function eliminar() 
	{
		$_clase = $this;
		// OBTENER INFORMACION DEL REGISTRO INSTANCIADO
		$_resultado = $this->_buscarCoincidenciaId($_clase);
		// CLASE INSTANCIADA COMO ARREGLO
		$_clase_objeto = $this->_utilitario->getClaseComoArreglo($_clase);
		// RETORNO DE LA FUNCION
		$_retorno = false;

		if($_resultado!=null && !empty($_resultado)) {
			if(array_key_exists("almacen", $_resultado)) {
				$_almacen = $_resultado["almacen"];
				$_registro_llave = $_resultado["posicion_registro"];
				$_clase_almacen_temporal = $_resultado["nombre_almacen"];

				if(!is_null($_registro_llave) && is_int($_registro_llave)) {
					// ELIMINACION DEL REGISTRO EN EL ALMACEN
					array_splice($_almacen, $_registro_llave, 1);

					// GUARDAR EN ARCHIVO
					$this->_utilitario->agregarArchivo($_clase_almacen_temporal, $_almacen);

					$_retorno = true;
				}
			}
		}

		return $_retorno;
	}

	private function _consultar($type, $filtro = null) 
	{
		$_clase = $this;
		$_resultado = ($type === "uno") ? new $_clase : [];
		
		// OBTENER NOMBRE DEL ALMACEN DE DATOS
		$_clase_almacen_temporal = $this->_utilitario->getNombreAlmacenTemporal($_clase);

		if(is_array($filtro)) {
			// OBETNER CONTENIDO DEL ARCHIVO
			$_almacen = $this->_utilitario->obtenerArchivo($_clase_almacen_temporal);

			// RECORRIDO DEL ARCHIVO
			foreach($_almacen as $_registro) {
				// VERIFICAR EL REGISTO ITERADO
				if($_registro!=null && !empty($_registro)) {
					// VARIFICAR SI TODOS LOS CAMPOS EN EL FILTRO SE ENCUENTRAB EN EL REGISTRO
					$_propiedad_varificada = [];

					// RECORRIDO DE LOS VALORES DEL REGISTRO
					foreach($_registro as $_propiedad => $_valor) {
						// FORMATO DE LA PROPIEDAD EN EL ARCHIVO (SOLO MINUSCULAS | SOLO LETRAS)
						$_propiedad_formato = $this->_utilitario->getPropiedadFormat($_propiedad);

						// RECORRIDO DE LOS VALORES DEL FILTRO
						foreach($filtro as $_propiedad_filtro => $_valor_filtro) {
							if(!array_key_exists($_propiedad_filtro, $_propiedad_varificada)) {
								$_propiedad_varificada[$_propiedad_filtro] = false;
							}

							// VERIFICAR LAS COINCIDENCIAS SEGUN EL FILTRO
							if(($_propiedad_filtro == $_propiedad_formato) && ($_valor_filtro == $_valor)) {
								// SI EN EL FILTRO EXISTEN COINCIDENCIAS CON LOS DATOS (PROPIEDADES | VALOR) DEL REGISTRO
								$_propiedad_varificada[$_propiedad_filtro] = true;
								break;
							}
						}
					}

					// ASIGNAR EL VALOR FINAL DE LAS VERIFICACIONES (TODAS LAS PROPIEDADES Y VALORES DEL FILTRO COINCIDEN)
					$_valores_verificados_exitosamente = !in_array(false, $_propiedad_varificada, true);

					// SI TODAS LAS VERIFICACIONES SON CORRECTAS
					if($_valores_verificados_exitosamente) {
						// NUEVA INSTANCIA DE LA CLASE
						$_clase_instancia = new $_clase;

						// ASIGNAR VALOR A UNA NUEVA INSTANCIA DE LA CLASE
						foreach($_registro as $_propiedad => $_valor) {
							// FORMATO DE LA PROPIEDAD EN LA CLASE
							$_propiedad_format_clase = '_' . $_propiedad;

							$_clase_instancia->$_propiedad_format_clase = $_valor;
						}

						if($type === "uno") {
							// ASIGNAR REGISTRO VERIFICADO
							$_resultado = $_clase_instancia;
							break;
						} else if($type === "todos") {
							// ASIGNAR REGISTROS VERIFICADO
							$_resultado[] = $_clase_instancia;
						}
					}
				}
			}
		} else if(!isset($filtro)) {
			// OBETNER CONTENIDO DEL ARCHIVO
			$_almacen = $this->_utilitario->obtenerArchivo($_clase_almacen_temporal);
                        
            // RECORRIDO DEL ARCHIVO
			foreach($_almacen as $_registro) {
				// VERIFICAR EL REGISTO ITERADO
                if($_registro!=null && !empty($_registro)) {
                    // NUEVA INSTANCIA DE LA CLASE
                    $_clase_instancia = new $_clase;

                    // ASIGNAR VALOR A UNA NUEVA INSTANCIA DE LA CLASE
                    foreach($_registro as $_propiedad => $_valor) {
                            // FORMATO DE LA PROPIEDAD EN LA CLASE
                            $_propiedad_format_clase = '_' . $_propiedad;

                            $_clase_instancia->$_propiedad_format_clase = $_valor;
                    }

                    if($type === "uno") {
                            // ASIGNAR REGISTRO VERIFICADO
                            $_resultado = $_clase_instancia;
                            break;
                    } else if($type === "todos") {
                            // ASIGNAR REGISTROS VERIFICADO
                            $_resultado[] = $_clase_instancia;
                    }
                }
        	}
		}

		return $_resultado;
	}

	private function _buscarCoincidenciaId($_clase)
	{
		// OBTENER NOMBRE DEL ALMACEN DE DATOS
		$_clase_almacen_temporal = $this->_utilitario->getNombreAlmacenTemporal($_clase);
		// CLASE INSTANCIADA COMO ARREGLO
		$_clase_objeto = $this->_utilitario->getClaseComoArreglo($_clase);
		// RESULTADO
		$_resultado = [
			"encontrado" => false,
			"nombre_almacen" => $_clase_almacen_temporal,
			"almacen" => [],
			"registro" => null,
			"posicion_registro" => null,
		];
        $_verificacion_existencia = false;

		if($_clase_objeto!=null && !empty($_clase_objeto)) {
			$_anotacion_id = $this->_utilitario->getClaseIdParametro($_clase);
			$_anotacion_id_format = $this->_utilitario->getPropiedadFormat($_anotacion_id); 

			if(array_key_exists($_anotacion_id_format, $_clase_objeto)) {
				$_clase_objeto_id_valor = $_clase_objeto[$_anotacion_id_format];

				// OBETNER CONTENIDO DEL ARCHIVO
				$_almacen = $this->_utilitario->obtenerArchivo($_clase_almacen_temporal);
                                
                $_resultado["almacen"] = $_almacen;

				// RECORRIDO DEL ARCHIVO
				foreach($_almacen as $_registro_llave => $_registro) {
                                    
					// VERIFICAR EL REGISTO ITERADO
					if($_registro!=null && !empty($_registro)) {

						// RECORRIDO DE LOS VALORES DEL REGISTRO
						foreach($_registro as $_propiedad => $_valor) {
							// FORMATO DE LA PROPIEDAD EN EL ARCHIVO (SOLO MINUSCULAS | SOLO LETRAS)
							$_propiedad_formato = $this->_utilitario->getPropiedadFormat($_propiedad);

							// VERIFICAR LAS COINCIDENCIAS SEGUN EL FILTRO
							if(($_anotacion_id_format == $_propiedad_formato) && ($_clase_objeto_id_valor == $_valor)) {
								// REGISTRO ENCONTRADO
                                $_verificacion_existencia = true;
                                $_resultado["encontrado"] = true;
                                $_resultado["registro"] = $_registro;
                                $_resultado["posicion_registro"] = $_registro_llave;
								break;
							}
						}
					}
                                        
                    if($_verificacion_existencia) {
                        break;
                    }
				}
			}
		}

		return $_resultado;
	}
}