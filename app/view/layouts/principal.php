<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>GeoPagos - Prueba</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <div class="page-header">
                <h1>Página Principal</h1>
                <p class="lead">
                    Prueba de Ingreso - Ramón Ramírez
                </p>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div style="background-color: #eceeef; padding: 4rem 2rem; margin-bottom: 2rem; border-radius: .3rem;">
                        <?php include $_contenido; ?>
                    </div>
                </div>
            </div>
        </div>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>