<div class="">
    <h2> Favoritos del Usuario: <?= $usuario->getUsuario() ?> </h2>
    
    <p class="lead">
        Agregar favorito
        <a href="crear/<?= $usuario->getCodigoUsuario() ?>" class="btn btn-primary"> Agregar </a>
    </p>
    
    <table class="table">
        <thead>
            <tr>
                <th> Código </th>
                <th> Nombre </th>
                <th> Edad </th>
                <th> Acciones </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($modelo as $_llave => $_usuario) { ?>
                <tr>
                    <th> <?= $_usuario->getCodigoUsuario() ?> </th>
                    <th> <?= $_usuario->getUsuario() ?> </th>
                    <th> <?= $_usuario->getEdad() ?> </th>
                    <th>
                        <a href="eliminar/<?= $_usuario->getCodigoUsuario() ?>" class="btn btn-danger">
                            Eliminar
                        </a>
                    </th>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>