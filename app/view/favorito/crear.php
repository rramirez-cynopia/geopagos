<div class="">
    <h2>Agregar Usuario Favorito</h2>
    
    <form action="<?= $usuario->getCodigoUsuario() ?>" method="POST">
        <div class="form-group">
            <label for="usuario">
                Usuarios
            </label>
            <select class="form-control" id="codigousuariofavorito" name="codigousuariofavorito">
                <?php foreach ($usuarios as $_llave => $_usuario) { ?>
                    <option value="<?= $_usuario->getCodigoUsuario() ?>"><?= $_usuario->getUsuario() ?></option>
                <?php } ?>
            </select>
        </div>

        <button type="submit" class="btn btn-success">
            Guardar
        </button>
    </form>
</div>