<div class="">
    <h2>Nuevo Usuario</h2>
    
    <form action="" method="POST">
        <div class="form-group">
            <label for="usuario">
                Nombre
            </label>
            <input type="text" class="form-control" id="usuario" name="usuario">
        </div>
        <div class="form-group">
            <label for="clave">
                Clave
            </label>
            <input type="password" class="form-control" id="clave" name="clave">
        </div>
        <div class="form-group">
            <label for="edad">
                Edad
            </label>
            <input type="number" class="form-control" id="edad" name="edad">
        </div>

        <button type="submit" class="btn btn-success">
            Guardar
        </button>
    </form>
</div>