<div class="">
    <h2>Usuario #<?= $modelo->getCodigoUsuario() ?></h2>
    
    <form action="<?= $modelo->getCodigoUsuario() ?>" method="POST">
        <input type="hidden" name="usuario" value="<?= $modelo->getUsuario() ?>">

        <div class="form-group">
            <h3>
                Nombre: 
            </h3>

            <?= $modelo->getUsuario() ?>
        </div>

        <div class="form-group">
            <h3>
                Edad: 
            </h3>

            <?= $modelo->getEdad() ?>
        </div>

        <a href="<?= $pagina_index ?>" class="btn btn-default">
            Cancelar
        </a>
        <button type="submit" class="btn btn-danger">
            Eliminar
        </button>
    </form>
</div>