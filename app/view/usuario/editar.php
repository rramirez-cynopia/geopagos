<div class="">
    <h2>Usuario #<?= $modelo->getCodigoUsuario() ?></h2>
    
    <form action="<?= $modelo->getCodigoUsuario() ?>" method="POST">
        <div class="form-group">
            <label for="usuario">
                Nombre
            </label>
            <input type="text" class="form-control" id="usuario" name="usuario" value="<?= $modelo->getUsuario() ?>">
        </div>
        <div class="form-group">
            <label for="clave">
                Clave
            </label>
            <input type="password" class="form-control" id="clave" name="clave" value="<?= $modelo->getClave() ?>">
        </div>
        <div class="form-group">
            <label for="edad">
                Edad
            </label>
            <input type="number" class="form-control" id="edad" name="edad" value="<?= $modelo->getEdad() ?>">
        </div>

        <a href="<?= $pagina_index ?>" class="btn btn-default">
            Cancelar
        </a>
        <button type="submit" class="btn btn-success">
            Guardar
        </button>
    </form>
</div>