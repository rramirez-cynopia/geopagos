<div class="">
    <h2> Usuarios </h2>
    
    <p class="lead">
        Crear nuevo usuario
        <a href="crear" class="btn btn-primary"> Crear </a>
    </p>
    
    <table class="table">
        <thead>
            <tr>
                <th> Código </th>
                <th> Nombre </th>
                <th> Edad </th>
                <th> Acciones </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($modelo as $_llave => $_usuario) { ?>
                <tr>
                    <th> <?= $_usuario->getCodigoUsuario() ?> </th>
                    <th> <?= $_usuario->getUsuario() ?> </th>
                    <th> <?= $_usuario->getEdad() ?> </th>
                    <th>
                        <a href="editar/<?= $_usuario->getCodigoUsuario() ?>" class="btn btn-success">
                            Editar
                        </a>
                        <a href="favoritos/<?= $_usuario->getCodigoUsuario() ?>" class="btn btn-default">
                            Favoritos
                        </a>
                        <a href="pagos/<?= $_usuario->getCodigoUsuario() ?>" class="btn btn-primary">
                            Pagos
                        </a>
                        <a href="eliminar/<?= $_usuario->getCodigoUsuario() ?>" class="btn btn-danger">
                            Eliminar
                        </a>
                    </th>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>